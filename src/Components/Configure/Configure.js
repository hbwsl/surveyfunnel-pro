const { addFilter, addAction } = wp.hooks;
import React from 'react';

addFilter( 'configureProState', 'Configure', configureProState )

function configureProState() {
	return {
		privacyPolicy: {
			text: '',
			link: { label: '', value: '' },
		}
	}
}

addFilter( 'renderPrivacyPolicySettings', 'Configure', renderPrivacyPolicySettings );

function renderPrivacyPolicySettings (data, setProSettings, proSettings, fetchData, Select, options, setOptions) {
	const handlePrivacyPolicyChange = (e) => {
		setProSettings({
			...proSettings,
			privacyPolicy: {
				...proSettings.privacyPolicy,
				[e.target.name]: e.target.value,
			}
		});
	}
	let defaultValue = {};
	options.forEach((posts) => {
		for(let i = 0 ; i < posts.options.length ; i++) {
			if ( posts.options[i].value === proSettings.privacyPolicy.link.value ) {
				defaultValue = posts.options[i];
				return;
			}
		}
	})

	const optionsChange = ( data ) => {
		let newProSettings = JSON.parse(JSON.stringify(proSettings));
		newProSettings.privacyPolicy.link = data;
		setProSettings(newProSettings);
	}

	return (
		
		<div className="privacy-policy-container">
			<h3>Set Privacy Policy</h3>
			<p>Ensure users accept your privacy policy before starting the survey.</p>
			
			<label htmlFor="description">Privacy Policy Page: </label>
			<Select
				options={options}
				value={defaultValue}
				onChange={optionsChange}
			>
			</Select>

			<label htmlFor="text">Text: </label>
			<input type="text" id="text" placeholder="Accept Privacy Policy" value={proSettings.privacyPolicy.text} name="text" onChange={handlePrivacyPolicyChange} />
		</div>
	);
}

addAction( 'configureMount', 'Configure', configureMount );

function configureMount ( configure, setProSettings ) {
	if ( configure?.proSettings ) {
		setProSettings(configure.proSettings);
	}
}

let exportFunctionsForTesting = { configureMount, renderPrivacyPolicySettings, configureProState };
export default exportFunctionsForTesting;