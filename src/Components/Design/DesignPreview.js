const { addFilter } = wp.hooks;
import React from 'react';

addFilter('renderPrivacyPolicy', 'DesignPreview', renderPrivacyPolicy)
addFilter('changeCurrentTabAsPerConditionalLogic', 'DesignPreview', changeCurrentTabAsPerConditionalLogic)

function changeCurrentTabAsPerConditionalLogic(num, componentList, currentTab) {
	if(componentList[currentTab].type === 'START_ELEMENTS') {
		return num;
	}
	let {conditions} = componentList[currentTab];
	if(!conditions) {
		return num;
	}
	const conditionsLength = conditions.length;
	const componentListLength = componentList.length;
	let currentIndex = 0;
	for(let i=0; i < componentListLength; i++) {
		if(componentList[currentTab].id === componentList[i].id) {
			currentIndex = i;
			break;
		}
	}
	switch(componentList[currentTab].componentName) {
		case 'SingleChoice':
		case 'ImageQuestion':
			for(let i=0; conditionsLength; i++) {
				if(!conditions[i]?.conditionOn) {
					continue;
				}
				if(conditions[i].conditionOn) {
					if(componentList[currentTab].value === conditions[i].jumpOption) {
						let jumpQuestionId = conditions[i].jumpQuestion.id;
						for(let j=0; j<componentListLength; j++) {
							if(componentList[j].id === jumpQuestionId) {
								if(j < currentIndex) {
									j = j-currentIndex;
								}
								return j;
							}
						}
					}
				}
			}
			break;
		case 'MultiChoice':
			let selectedAnswers = [];
			for(let i=0;i<componentList[currentTab].answers.length;i++) {
				if(componentList[currentTab].answers[i].checked) {
					selectedAnswers.push(componentList[currentTab].answers[i].name)
				}
			}
			if(selectedAnswers.length === 0) {
				return num;
			}
			for(let i=0; i<conditionsLength; i++) {
				if(!conditions[i]?.conditionOn) {
					continue;
				}
				if(conditions[i].jumpCondition === 'any') {
					for(let k=0; k < conditions[i].jumpOption.length; k++) {
						if(selectedAnswers.includes(conditions[i].jumpOption[k])) {
							for(let j=0; j<componentListLength; j++) {
								if(componentList[j].id === conditions[i].jumpQuestion.id) {
									if(j < currentIndex) {
										j = j-currentIndex;
									}
									return j;
								}
							}
						}
					}
				}else {
					let flag = 0;
					if(selectedAnswers.length === conditions[i].jumpOption.length) {
						for(let k=0; k<conditions[i].jumpOption.length; k++) {
							if(!selectedAnswers.includes(conditions[i].jumpOption[k])) {
								flag = 1;
								break;
							}
						}
						if(flag === 0) {
							for(let j=0; j<componentListLength; j++) {
								if(componentList[j].id === conditions[i].jumpQuestion.id) {
									if(j < currentIndex) {
										j = j-currentIndex;
									};
									return j;
								}
							}
						}
					}
				}
			}
			break;
		default: 
		for(let i=0; conditionsLength; i++) {
			if(!conditions[i]?.conditionOn) {
				continue;
			}
			if(conditions[i].conditionOn) {
				let jumpQuestionId = conditions[i].jumpQuestion.id;
				for(let j=0; j<componentListLength; j++) {
					if(componentList[j].id === jumpQuestionId) {
						if(j < currentIndex) {
							j = j-currentIndex;
						}
						return j;
					}
				}
			}
		}
		return num;
	}
	return num;
}

function validateImageUrl( url ) {
	return url.match(/^http.*\.(jpeg|jpg|png)$/) != null
}

function renderPrivacyPolicy( data, item, proSettings, src ) {
	let disabled = false;
	if ( proSettings.privacyPolicy.text === '' || proSettings.privacyPolicy.link?.value === '' ) {
		disabled = true;
	}
	let privacyPolicy = item?.privacyPolicy && ! disabled;
	return <>
		{privacyPolicy ? <div className="privacyPolicy">
			<p><input type="checkbox" id="privacyPolicyEnabled" /><label htmlFor="privacyPolicyEnabled">
					<span>
						<img src={src} alt="Privacy Policy Checkbox" />
					</span>
					</label> {proSettings.privacyPolicy.text} <a href={proSettings.privacyPolicy.link.value} target="_blank">Learn More</a></p>
		</div> : ''}
	</>
}

addFilter( 'renderContentElementsDesignPreview', 'DesignPreview', renderContentElementsDesignPreview )

function renderContentElementsDesignPreview( data, item, style, convertToRgbaCSS, changeCurrentTab, designCon, handleRadioChange, index ) {
	switch( item.componentName ) {
		case 'TextElement':
			return (
				<div className={"surveyfunnel-lite-tab-" + item.componentName}
				style={{ ...style }}
				key={item.id}
				>
					<div
						className="tab-container"
						key={item.id}
					>
						<div className="tab" tab-componentname={item.componentName}>
							<h3 className="surveyTitle">{item.title}</h3>
							<p className="surveyDescription">{item.description}</p>
							<button type="button" className="surveyButton" style={{ background: convertToRgbaCSS(designCon.buttonColor), color: convertToRgbaCSS(designCon.buttonTextColor) }} onClick={() => {
									changeCurrentTab(1);
							}}>Next</button>
						</div>
					</div>
				</div>
			);
		case 'ImageQuestion':
			return (
				<div className={"surveyfunnel-lite-tab-" + item.componentName}
					style={{ ...style }}
					key={item.id}
				>
					<div
						className="tab-container"
						key={item.id}
					>
						<div className="tab" tab-componentname={item.componentName}>
							<h3 className="surveyTitle">{item.title}</h3>
							<p className="surveyDescription">{item.description}</p>
							<div className="imageAnswerFlexBox">
							{ item.answers.map(function(answer, idx) {
									return validateImageUrl(answer.imageUrl) ? (<label key={idx} style={{ border: `1px solid ${convertToRgbaCSS(designCon.answerBorderColor)}` }} className={"surveyfunnel-lite-tab-imageanswer-container" + ( item.value === answer.name ? ' active' : '' )}>
												<input id={`${idx}_${answer.name}_single`} type="radio" name="radiogroup" value={answer.name} onChange={handleRadioChange} listidx={index} />
												<div htmlFor={`${idx}_${answer.name}_single`}>
													<div className="image-answer">
														<img src={answer.imageUrl} alt={`image${idx}`} />
													</div>
													<div className='imageAnswerName'>
														{answer.name}
													</div>
												</div>
											</label>) : ''}
								)}
							</div>
							<button type="button" className="surveyButton" style={{ background: convertToRgbaCSS(designCon.buttonColor), color: convertToRgbaCSS(designCon.buttonTextColor) }} onClick={() => {
									changeCurrentTab(1);
							}}>Next</button>
						</div>
					</div>
				</div>
			)
	}
	return '';
}

addFilter( 'callRenderContentElements', 'DesignPreview', callRenderContentElements );

function callRenderContentElements( data, renderElements, item, i ) {
	switch( item.componentName ) {
		case "TextElement":
		case 'ImageQuestion':
			return renderElements(item, 'block', i);
		default:
			return '';
	}
}

let exportFunctionsForTesting = { renderPrivacyPolicy, renderContentElementsDesignPreview, callRenderContentElements, changeCurrentTabAsPerConditionalLogic };
export default exportFunctionsForTesting;