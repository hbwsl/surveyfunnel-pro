const { addFilter } = wp.hooks;
import React from 'react';

addFilter( 'addCoverPageStateElements', 'StartScreenElements', addCoverPageStateElements );

function addCoverPageStateElements ( state ) {
	state = {
		...state,
		privacyPolicy: false,
	};
	return state;
}

addFilter( 'startScreenLeftElements', 'StartScreenElements', startScreenLeftElements )

function startScreenLeftElements( data ,handleChange, state, proSettings ) {
	let disabled = false;
	if ( proSettings.privacyPolicy.text === '' || proSettings.privacyPolicy.link?.value === '' ) {
		disabled = true;
	}
	
	return (<div className="modalComponentPrivacyPolicy">
		<h3>Enable Privacy Policy</h3>
		<input
			type="checkbox"
			name="privacyPolicy"
			id="privacyPolicy"
			checked={state?.privacyPolicy && ! disabled}
			onChange={(e) => {handleChange(e, true)}}
			disabled={disabled}
		/>
		<label htmlFor="privacyPolicy"></label>
		{disabled && <p>Kindly navigate to <a href="#/configure">Configure Tab</a> and set the Privacy Policy to enable this option.</p>}
	</div>);
}

addFilter( 'componentDidMountStartScreen', 'StartScreenElements', componentDidMountStartScreen );

function componentDidMountStartScreen( data ,currentElement ) {
	return {
		privacyPolicy: currentElement?.privacyPolicy || false,
	}
}

addFilter( 'startScreenRightPrivacyPolicy', 'StartScreenElements', startScreenRightPrivacyPolicy )

function startScreenRightPrivacyPolicy( data, state, proSettings, src ) {
	let disabled = false;
	if ( proSettings.privacyPolicy.text === '' || proSettings.privacyPolicy.link?.value === '' ) {
		disabled = true;
	}
	let privacyPolicy = state?.privacyPolicy && ! disabled;
	return <>
		{privacyPolicy ? <div className="privacyPolicy">
			<p><input type="checkbox" id="privacyPolicyEnabled" /><label htmlFor="privacyPolicyEnabled">
					<span>
						<img src={src} alt="Privacy Policy Checkbox" />
					</span>
				</label> {proSettings.privacyPolicy.text} <a href={proSettings.privacyPolicy.link.value} target="_blank">Learn More</a></p>
		</div> : ''}
	</>
}

let exportFunctionsForTesting = { addCoverPageStateElements, startScreenLeftElements, startScreenRightPrivacyPolicy, componentDidMountStartScreen };
export default exportFunctionsForTesting;