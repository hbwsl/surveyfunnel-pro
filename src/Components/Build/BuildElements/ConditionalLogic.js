import React from "react";
import AddCondition from "./AddCondition";
import ConditionsElement from "./ConditionsElement";
import "../../../scss/conditionalLogic.scss";

export default class ConditionalLogic extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			conditions: []
		}
		this.addConditionToState = this.addConditionToState.bind(this);
		this.onSaveHandler = this.onSaveHandler.bind(this);
		this.getConditionalOptions = this.getConditionalOptions.bind(this);
		this.conditionChangeHandler = this.conditionChangeHandler.bind(this);
		this.deleteCondition = this.deleteCondition.bind(this);
		this.addMultiCondition = this.addMultiCondition.bind(this);
		this.conditionalMultiChangeHandler = this.conditionalMultiChangeHandler.bind(this);
		this.deleteMultiCondition = this.deleteMultiCondition.bind(this);
	}
	componentDidMount() {
		const {currentElement} = this.props;
		if(currentElement.data.conditions && currentElement.data.conditions.length > 0) {
			this.setState({
				conditions: currentElement.data.conditions
			})
		} 
		currentElement.data.conditions = this.state.conditions
	}
	deleteCondition(conditionIndex) {
		const newConditions = [...this.state.conditions]
		newConditions.splice(conditionIndex, 1)
		this.setState({
			conditions: newConditions
		})
	}
	conditionChangeHandler(property, value, conditionIndex) {
		const newCondition = this.state.conditions[conditionIndex]
		if(property === 'jumpQuestion') {
			const values = value.split(',')
			newCondition[property].id = values[0]
			newCondition[property].type = values[1]
			newCondition[property].title = values[2]
			newCondition[property].componentName = values[3]
		}
		else {
			newCondition[property] = value
		}
		const newConditions = [...this.state.conditions]
		newConditions[conditionIndex] = newCondition
		this.setState({
			conditions: newConditions
		})
	}
	conditionalMultiChangeHandler(value, conditionIndex, optionIndex) {
		let newConditions = this.state.conditions;
		newConditions[conditionIndex].jumpOption[optionIndex] = value;
		this.setState({
			conditions: newConditions
		}) 
	}
	getConditionalOptions() {
		switch(this.props.currentElement.data.componentName) {
			case 'SingleChoice':
			case 'ImageQuestion':
				if(this.state.conditions.length === 0) {
					return <AddCondition buttonClass="additionalConditionButton" addConditionToState={this.addConditionToState}/>
				}
				return (
					<>
					{this.state.conditions.map((condition, index) => <ConditionsElement conditionalMultiChangeHandler={this.conditionalMultiChangeHandler} deleteMultiCondition ={this.deleteMultiCondition} addMultiCondition={this.addMultiCondition} key={index} deleteCondition={this.deleteCondition} list={this.props.List} condition={condition} conditionIndex={index} currentElement={this.props.currentElement} handler={this.conditionChangeHandler}  key={index} type="SingleChoice"/>)}
					<AddCondition buttonClass="additionalConditionButton" addConditionToState={this.addConditionToState}/>
					</>
					);
			case 'MultiChoice':
				if(this.state.conditions.length === 0) {
					return <AddCondition buttonClass="additionalConditionButton" addConditionToState={this.addConditionToState}/>
				}
				return (
					<>
					{this.state.conditions.map((condition, index) => <ConditionsElement conditionalMultiChangeHandler={this.conditionalMultiChangeHandler} deleteMultiCondition ={this.deleteMultiCondition} addMultiCondition={this.addMultiCondition} key={index} deleteCondition={this.deleteCondition} list={this.props.List} condition={condition} conditionIndex={index} currentElement={this.props.currentElement} handler={this.conditionChangeHandler} key={index} type="MultiChoice"/>)}
					<AddCondition buttonClass="additionalConditionButton" addConditionToState={this.addConditionToState}/>
					</>
					);
			default:
				if(this.state.conditions.length === 0) {
					return <AddCondition buttonClass="additionalConditionButton" addConditionToState={this.addConditionToState}/>
				}
				return <ConditionsElement conditionalMultiChangeHandler={this.conditionalMultiChangeHandler} deleteMultiCondition ={this.deleteMultiCondition} addMultiCondition={this.addMultiCondition} deleteCondition={this.deleteCondition} list={this.props.List} condition={this.state.conditions[0]} conditionIndex={0} currentElement={this.props.currentElement} handler={this.conditionChangeHandler} type="NoOption"/>
		}
	}
	addMultiCondition(conditionIndex) {
		if(this.state.conditions[conditionIndex].jumpOption.length >= this.props.currentElement.data.answers.length) {
			return;
		}
		let newConditions = this.state.conditions;
		let defaultOption = this.props.currentElement.data.answers.find((answer) => !newConditions[conditionIndex].jumpOption.includes(answer.name) )
		newConditions[conditionIndex].jumpOption.push(defaultOption.name)
		this.setState({
			conditions: newConditions
		})
	}
	deleteMultiCondition(conditionIndex, optionIndex) {
		let newConditions = this.state.conditions;
		newConditions[conditionIndex].jumpOption.splice(optionIndex, 1);
		this.setState({
			conditions: newConditions
		})
	}
	addConditionToState() {
		let newCondition = {};
		const newConditions = this.state.conditions
		let jumpQuestionIndex =  0;
		for( let i=0; i < this.props.List.CONTENT_ELEMENTS.length; i++ ) {
			if( this.props.List.CONTENT_ELEMENTS[i].id !== this.props.currentElement.data.id ) {
				jumpQuestionIndex = i;
				break;
			}
		}
		switch(this.props.currentElement.data.componentName) {
			case 'SingleChoice':
			case 'ImageQuestion':
				newCondition = {
					jumpCondition: null,
					jumpQuestion: {
						id: this.props.List.CONTENT_ELEMENTS[jumpQuestionIndex].id,
						type: this.props.List.CONTENT_ELEMENTS[jumpQuestionIndex].type,
						title: this.props.List.CONTENT_ELEMENTS[jumpQuestionIndex].title
					},
					jumpOption: this.props.currentElement.data.answers[0].name,
					conditionOn: true
				}
				newConditions.push(newCondition)
				this.setState({conditions: newConditions})
				return
			case 'MultiChoice':
				newCondition = {
					jumpCondition: 'any',
					jumpQuestion: {
						id: this.props.List.CONTENT_ELEMENTS[jumpQuestionIndex].id,
						type: this.props.List.CONTENT_ELEMENTS[jumpQuestionIndex].type,
						title: this.props.List.CONTENT_ELEMENTS[jumpQuestionIndex].title
					},
					jumpOption: [this.props.currentElement.data.answers[0].name],
					conditionOn: true
				}
				newConditions.push(newCondition)
				this.setState({conditions: newConditions})
				return
			default:
				newCondition = {
					jumpCondition: null,
					jumpQuestion: {
						id: this.props.List.CONTENT_ELEMENTS[jumpQuestionIndex].id,
						type: this.props.List.CONTENT_ELEMENTS[jumpQuestionIndex].type,
						title: this.props.List.CONTENT_ELEMENTS[jumpQuestionIndex].title
					},
					jumpOption: null,
					conditionOn: true
				}
				newConditions.push(newCondition)
				this.setState({conditions: newConditions})
				return
		}
	}
	onSaveHandler() {
		this.props.currentElement.data.conditions = this.state.conditions;
		this.props.editList(this.props.currentElement.data);
		this.props.setCurrentElement(null);
		this.props.setShowModal(false);
	}
	render() {
		const {currentElement, CloseModal} = this.props;
		return (
			<>
			<div className="modalOverlayConditionalBranching">
				<div className="modalContent-navbar">
					<h3>Conditional Branching</h3>
					<CloseModal/>
				</div>
				<div className="modalContent">
					<div className="modalContent-single">
						<h3 className="modalQuestionName">{currentElement.data.title}</h3>
						<div>
						{this.getConditionalOptions()}
						</div>
					</div>
				</div>
				<div className="modalContent-footer">
					<AddCondition buttonClass="modalContent-footer-primary-button" addConditionToState={this.addConditionToState} />
					<div>
						<button className="modalContent-footer-primary-button" onClick={this.onSaveHandler}>
							Save
						</button>
						<CloseModal buttonClass="modalContent-footer-secondary-button" closeText="Cancel"/>
					</div>
				</div>
			</div>
			</>
		);
	}
}