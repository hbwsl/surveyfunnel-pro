import React from "react";
const { applyFilters } = wp.hooks;
let index = null;
import '../../../scss/imageQuestion.scss';

function validateImageUrl( url ) {
	return url.match(/^http.*\.(jpeg|jpg|png)$/) != null
}

export default class ImageQuestion extends React.Component {
    state = {
        title: "",
        description: "",
        answers: [
            {
                name: "",
                checked: false,
                ...applyFilters("choicesState", {}, this.props.type),
				imageUrl: '',
            },
			{
                name: "",
                checked: false,
                ...applyFilters("choicesState", {}, this.props.type),
				imageUrl: '',
            },
        ],
        value: "",
        mandatory: false,
        error: "",
		proVersionQuestionType: true,
    };

	handleImageChange = (value) => {
		const newAnswers = this.state.answers.map((answer, sidx) => {
			if (index !== sidx) return answer;
			return { ...answer, imageUrl: value, error: '' };
		});

		this.setState({ answers: newAnswers });
	};

	componentDidMount() {
		const { currentElement } = this.props;

		if (currentElement?.answers && "currentlySaved" in currentElement) {
			let state = {
				title: currentElement.title,
				description: currentElement.description,
				answers: currentElement.answers,
				mandatory: currentElement?.mandatory,
			};
			this.setState(state);
		}

		let _this = this;

		window.send_to_editor = function(html) {
					
			var doc = new DOMParser().parseFromString(html, "text/xml");
			let image = doc.querySelector('img');
			var imgurl = '';
			if ( image ) {
				imgurl = image.getAttribute('src');
			}
			tb_remove();

			if ( validateImageUrl( imgurl ) ) {
				_this.handleImageChange(imgurl);
			}
			else {
				this.setState({
					error: 'Select Only Image of type JPEG, JPG, PNG',
				})
			}
			index = null;
		}
	}

	handleAddAnswer = () => {
		this.setState({
			answers: this.state.answers.concat([{ name: "", checked: false, ...applyFilters( 'choicesState', {},this.props.type ), imageUrl: '' }]),
		});
	};

	handleRemoveAnswer = (idx) => () => {
		this.setState({
			answers: this.state.answers.filter((s, sidx) => idx !== sidx),
		});
	};

	handleAnswerChange = (idx, key) => (evt) => {
		const newAnswers = this.state.answers.map((answer, sidx) => {
			if (idx !== sidx) return answer;
			return { ...answer, [key]: evt.target.value };
		});

		this.setState({ answers: newAnswers });
	};

	handleChange = (event) => {
		this.setState({
			[event.target.name]: event.target.value,
		});
	};

	checkForEmpty( name ) {
		if ( this.state[name] === '' ) {
			return false;
		}
		return true;
	}

	currentElementName( componentName ) {
		return 'Image Question';
	}

	handleSave = () => {
		let err = '';
		if ( this.state.answers.length >= 2 && this.state.title !== '' ) {
			err = '';
		}
		else if ( this.state.answers.length < 2 ) {
			err = 'Add atleast two answers.';
		}
		else {
			err = 'Please add title before saving.';
		}

		for(let i = 0; i < this.state.answers.length; i++) {
			if ( ! validateImageUrl(this.state.answers[i].imageUrl) ) {
				err = "Please add images for all the answers added!";
				break;
			}

			if ( this.state.answers[i].name === '' ) {
				err = "Please add text for all the answers added!";
				break;
			}
		}

		this.setState({
			error: err
		}, () => {
			let div = document.querySelector('.modalContent-left-fields');
			if ( div ) {
				div.lastElementChild.scrollIntoView({ behavior: 'smooth' });
			}
			
			if ( err === '' ) {
				this.props.saveToList();
			}
		})
	}

	handleAddAnswerImage = (idx) => (e) => {
		index = idx;
		tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
	}

    render() {
		const { currentElement, CloseModal, ModalContentRight, convertToRgbaCSS } = this.props;
		const { designCon, type } = this.props;
		return (	
			<>
				<div className="modalOverlay">
					<div className="modalContent-navbar">
						<h3>Content Elements &nbsp; &#62; &nbsp; {this.currentElementName(currentElement.componentName)} &nbsp; &#62; &nbsp; {this.state.title}</h3>
						<CloseModal/>
					</div>
					<div className="modalContent">
						<div className="modalContent-left">
							<div className="modalContent-left-fields">

								<div className="modalComponentTitle">
									<h3>Title</h3>
									<input
										type="text"
										placeholder="Set Title"
										name="title"
										value={this.state.title}
										onChange={this.handleChange}
									/>
								</div>
								<div className="modalComponentDescription">
									<h3>Description</h3>
									<textarea
										type="text"
										placeholder="Set Description"
										name="description"
										value={this.state.description}
										onChange={this.handleChange}
										style={{height:"150px"}}
									/>
								</div>
								<div className="modalComponentAnswers">
								<h3>Answers</h3>
								{this.state.answers.map((answer, idx) => (
									<div className="answers" key={idx}>
										<input
											type="text"
											placeholder={`Answer #${
												idx + 1
											}`}
											value={answer.name}
											onChange={this.handleAnswerChange(
												idx,
												'name'
											)}
										/>
										{applyFilters('answersLeftBoxes', '', type, answer, this.handleAnswerChange, idx)}
										<button
											type="button"
											onClick={this.handleAddAnswerImage(
												idx
											)}
											className="small"
										>
										<svg xmlns="http://www.w3.org/2000/svg" width="22.684" height="22.684" viewBox="0 0 22.684 22.684">
											<path id="Form_27" data-name="Form 27" d="M530.676,639.257a2.618,2.618,0,0,1-2.692-2.691c-.017-.15-.02-.342-.031-.506a5.085,5.085,0,0,1-1.72-.328,2.321,2.321,0,0,1-1.176-1.142c-.464-1.009-.4-2.2-.4-4.161v-8.1a28.037,28.037,0,0,1,.083-2.893,2.62,2.62,0,0,1,2.691-2.693,27.817,27.817,0,0,1,2.9-.084h8.1c.5,0,.945,0,1.353,0a6.47,6.47,0,0,1,2.856.421,2.323,2.323,0,0,1,1.123,1.228,5.283,5.283,0,0,1,.292,1.648c.166.011.358.012.509.029a2.62,2.62,0,0,1,2.693,2.691,27.769,27.769,0,0,1,.085,2.9v8.1a28.039,28.039,0,0,1-.083,2.893,2.621,2.621,0,0,1-2.691,2.693,28.065,28.065,0,0,1-2.9.084h-8.313A25.645,25.645,0,0,1,530.676,639.257Zm.179-17.664a1.087,1.087,0,0,0-1.263,1.264c-.068.6-.073,1.5-.073,2.714v8.1c0,1.212,0,2.11.073,2.714a1.088,1.088,0,0,0,1.265,1.263c.6.068,1.5.073,2.713.073h8.1c1.212,0,2.111,0,2.715-.073a1.088,1.088,0,0,0,1.263-1.265c.068-.6.072-1.5.072-2.713v-8.1c0-1.212,0-2.111-.072-2.715a1.087,1.087,0,0,0-1.265-1.262c-.6-.068-1.5-.073-2.714-.073h-8.1C532.357,621.52,531.459,621.525,530.855,621.593Zm-3.24-3.24a1.087,1.087,0,0,0-1.263,1.264c-.068.6-.073,1.5-.073,2.714v8.1a12.546,12.546,0,0,0,.25,3.485.5.5,0,0,0,.342.328,3.8,3.8,0,0,0,1.033.185c0-.254-.005-.473-.005-.758v-8.1a28.039,28.039,0,0,1,.083-2.893,2.62,2.62,0,0,1,2.691-2.693,27.8,27.8,0,0,1,2.9-.084h8.1c.284,0,.5,0,.755,0a4.153,4.153,0,0,0-.169-1,.513.513,0,0,0-.323-.365,12.307,12.307,0,0,0-3.5-.256h-8.1C529.117,618.28,528.219,618.285,527.615,618.353Zm15.1,16.7-3.482-3.482-1.294,1.275.252.251.022.023a.81.81,0,0,1-1.167,1.123l-.712-.714-.006,0a.811.811,0,0,1-.221-.221l-1.728-1.728-1.858,1.858-.022.022a.81.81,0,1,1-1.123-1.167l2.43-2.43a.811.811,0,0,1,1.146,0L536.8,631.7l1.877-1.848a.81.81,0,0,1,1.14,0l4.051,4.05.022.022a.81.81,0,1,1-1.167,1.124Zm-2.667-9.482a1.62,1.62,0,1,1,1.62,1.62A1.62,1.62,0,0,1,540.05,625.57Z" transform="translate(-524.656 -616.656)" fill={ validateImageUrl(answer.imageUrl) ? "#2196f3" : '#ADB8C1'}/>
										</svg>

										</button>
										<button
											type="button"
											onClick={this.handleRemoveAnswer(
												idx
											)}
											className="small"
											disabled={this.state.answers.length < 3}
										>
										<img src={require('../BuildImages/delete-icon.png')}></img>
										</button>
									</div>
								))}
								<button
									type="button"
									onClick={this.handleAddAnswer}
									className="modalAnswersAdd"
								>
									<img src={require('../BuildImages/add-icon.png')}></img>
									Add New Answer
								</button>
								</div>

								<div className="modalComponentMandatory">
									<h3>Set Question as mandatory</h3>
									<input type="checkbox" id="mandatory" name="mandatory" checked={this.state.mandatory} onChange={(e) => {
										this.setState({
											mandatory: !this.state.mandatory
										})
									}} />
									<label htmlFor="mandatory"></label>
								</div>
								<div className="modalComponentError">
									{this.state.error}
								</div>
							</div>
							<div className="modalComponentSaveButton">
								<button onClick={this.handleSave}>Save</button>
							</div>
						</div>
						<div className="modalContent-right">
						<div className="modalContentMode">
								<h4>Content Preview</h4>
							</div>
							<div className="modalContentPreview">
							{this.state.title === '' && this.state.description === '' && this.state.answers.length === 0 ? ( <div className="no-preview-available"><img src={require(`../BuildImages/unavailable.png`)}></img><div>No Preview Available</div></div> ) : (<ModalContentRight designCon={designCon} currentElement={currentElement.componentName}>
								{ this.checkForEmpty('title') && <h3 className="surveyTitle">{this.state.title}</h3> }
								{ this.checkForEmpty('description') && <p className="surveyDescription">{this.state.description}</p> }
								<div className="imageAnswerFlexBox">
								{ this.state.answers.map(function(answer, idx) {
									return validateImageUrl(answer.imageUrl) ? (<label key={idx} style={{ border: `0.45px solid ${convertToRgbaCSS(designCon.answerBorderColor)}` }} className="surveyfunnel-lite-tab-imageanswer-container">
												<input id={`${idx}_${answer.name}_single`} type="radio" name="radiogroup" value={answer.name} />
												<div htmlFor={`${idx}_${answer.name}_single`} className="imageAnswerBox">
													<div className="image-answer">
														<img src={answer.imageUrl} alt={`image${idx}`} />
													</div>
													<div className='imageAnswerName'>
														{answer.name}
													</div>
												</div>
											</label>) : ''}
								)}
								</div>
							</ModalContentRight>)}
							</div>
						</div>
					</div>
				</div>
			</>
		);
	}
}
