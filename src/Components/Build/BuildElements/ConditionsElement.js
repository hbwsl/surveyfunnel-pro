export default function ConditionsElement(props) {
	const {handler, conditionalMultiChangeHandler, condition, currentElement, type, conditionIndex, deleteMultiCondition, list, deleteCondition, addMultiCondition } = props
	const ELEMENTS = [...list.CONTENT_ELEMENTS,...list.RESULT_ELEMENTS]
	return(
		<>
		<div className="conditionalSettings">
		{type === 'MultiChoice' && (
			<div className="conditionalSetting">
				<div className="conditionalSettingsFlex">
					<div className="conditionalSetting-left" >
						<span>People who meet</span>
						<select defaultValue={condition.jumpCondition} onChange={(e) => handler('jumpCondition', e.target.value, conditionIndex)}>
							<option key={0} value="any">Any</option>
							<option key={1} value="all">All</option>
						</select>
						<span>of the following criteria:</span>
					</div>
					<div className="conditionSettings-right">
						<button className="conditionDeleteButton" onClick={() => deleteCondition(conditionIndex)}><img src={require('../BuildImages/delete-icon.png')} /></button>
						<div className="conditionStatus">
							<input id={"conditionCheck_" + conditionIndex} type="checkbox" checked={condition.conditionOn} onChange={(e) => handler('conditionOn', !condition.conditionOn, conditionIndex) } />
							<label htmlFor={"conditionCheck_" + conditionIndex}></label>
						</div>
					</div>
				</div>
				<div className="conditionalSetting">
					<p>If the answer to your question is:(you can add multiple answers)</p>
					{condition.jumpOption.map((option, index) => {
						return (
							<div className="conditionalSelectDiv" key={index}>
								<select className="conditionalSelectChoice selectMultiple" defaultValue={option} key={index} index={index} onChange={(e) => conditionalMultiChangeHandler(e.target.value, conditionIndex, index)} >
									{currentElement.data.answers.map((answer, index) =>  {
										return condition.jumpOption.includes(answer.name) && answer.name !== option ? '' : <option key={index} value={answer.name}>{answer.name}</option> ;
									} )} 
								</select>
								<button className="conditionalOptionAdd" onClick={() => addMultiCondition(conditionIndex)}><img src={require('../BuildImages/add-icon.png')} /></button>
								{index > 0 && <button className="conditionalOptionDelete" onClick={() => deleteMultiCondition(conditionIndex, index)}><img src={require('../BuildImages/delete-icon.png')} /></button>}
							</div>
						);
					})}
				</div>
			</div>
		)}
		{type === 'SingleChoice' && (
			<div className="conditionalSetting">
				<div className="conditionalSettingsFlex">
					<div className="conditionSetting-left">
						<p>If the answer to your question is:</p>
					</div>
					<div className="conditionSettings-right">
						<button className="conditionDeleteButton" onClick={() => deleteCondition(conditionIndex)}><img src={require('../BuildImages/delete-icon.png')} /></button>
						<div className="conditionStatus">
							<input id={"conditionCheck_" + conditionIndex} type="checkbox" checked={condition.conditionOn} onChange={(e) => handler('conditionOn', !condition.conditionOn, conditionIndex) } />
							<label htmlFor={"conditionCheck_" + conditionIndex}></label>
						</div>
					</div>
				</div>
				<div className="conditionalSelectDiv">
						<select className="conditionalSelectChoice" defaultValue={condition.jumpOption} onChange={(e) => handler('jumpOption', e.target.value, conditionIndex)}>
							{
								currentElement.data.answers.map((answer, index) => <option key={index} value={answer.name}>{answer.name}</option>)
							}
						</select>
				</div>
			</div>
		)}
		{
			type === 'NoOption' && (
				<div className="conditionalSetting">
					<div className="conditionalSettingsFlex">
						<div className="conditionalSetting-left">
							<p>Jump to:</p>
						</div>
						<div className="conditionSettings-right">
							<button className="conditionDeleteButton" onClick={() => deleteCondition(conditionIndex)}><img src={require('../BuildImages/delete-icon.png')} /></button>
							<div className="conditionStatus">
								<input id={"conditionCheck_" + conditionIndex} type="checkbox" checked={condition.conditionOn} onChange={(e) => handler('conditionOn', !condition.conditionOn, conditionIndex) } />
								<label htmlFor={"conditionCheck_" + conditionIndex}></label>
							</div>
						</div>
					</div>
					<div className="conditionalSelectDiv">
						<select className="conditionalSelectQuestion" defaultValue={condition.jumpQuestion.id+","+condition.jumpQuestion.type+","+condition.jumpQuestion.title.replace(/,/g,' ')+","+condition.jumpQuestion.componentName} onChange={(e) => handler('jumpQuestion', e.target.value, conditionIndex)}>
							{	
								ELEMENTS.map((element, index) => {
									return currentElement.data.title === element.title ? '' : <option key={index} value={element.id+","+element.type+","+element.title.replace(/,/g,' ')+","+element.componentName}>{element.componentName}-{element.title}</option>;
								})
							}
						</select>
					</div>
				</div>
			)
		}
		{
			type !== 'NoOption' && (
				<div className="conditionalSetting">
					<p>Jump to:</p>
					<div className="conditionalSelectDiv">
						<select className="conditionalSelectQuestion" defaultValue={condition.jumpQuestion.id+","+condition.jumpQuestion.type+","+condition.jumpQuestion.title.replace(/,/g,' ')+","+condition.jumpQuestion.componentName} onChange={(e) => handler('jumpQuestion', e.target.value, conditionIndex)}>
							{
								ELEMENTS.map((element, index) => currentElement.data.title === element.title ? '' : <option key={index} value={element.id+","+element.type+","+element.title.replace(/,/g,' ')+","+element.componentName}>{element.componentName}-{element.title}</option>)
							}
						</select>
					</div>
				</div>
			)
		}
		</div>
		</>
	);
}