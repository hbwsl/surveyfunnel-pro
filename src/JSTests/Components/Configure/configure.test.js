import testFunction from "../../../Components/Configure/Configure";

test( 'Configure Component State', () => {
	let data = testFunction.configureProState('');

	expect(data.privacyPolicy.text).toBe('');
	expect(data.privacyPolicy.link.label).toBe('');
	expect(data.privacyPolicy.link.value).toBe('');
} )

test( 'Configure already saved pro settings to be mounted', () => {
	let data = {};
	const setProSettings = (proSettings) => {
		data = proSettings;
	}

	let configure = {
		proSettings : {
			text: 'Hello world',
			link: {
				value: 'http://www.google.com',
				label: 'Privacy Policy',
			}
		}
	}

	testFunction.configureMount(configure, setProSettings);
	expect(data).toEqual(expect.objectContaining(configure.proSettings));
} );

test( 'Configuration Page Render Settings', () => {
	let data = {};
	const setProSettings = (proSettings) => {
		data = proSettings;
	}
	let proSettings = {
		privacyPolicy: {
			text: 'Hello World',
			link: {
				value: 'http://www.google.com',
				label: 'SRF Post Testing'
			}
		}
	}
	let options = [{
		value: 'Posts',
		options: [{
			value: 'http://www.example.com',
			label: 'Example Post'
		},
		{
			value: 'http://www.google.com',
			label: 'SRF Post Testing'
		}]
	}];
	let jsx = testFunction.renderPrivacyPolicySettings( '', setProSettings, proSettings, '', '', options, () => {} );
	expect(jsx.props.children[5].props.value).toBe(proSettings.privacyPolicy.text);
} )