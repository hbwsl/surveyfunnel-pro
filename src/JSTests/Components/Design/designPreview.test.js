import testFunctions from '../../../Components/Design/DesignPreview.js';
import React from 'react';

function ShowErrors() {
	return <div></div>
}

test( 'Start Screen Privacy Policy checkbox render in modalbox', () => {
	let data = '';
	let proSettings = {
		privacyPolicy: {
			text: 'something',
			link: {
				value: 'http://www.google.com'
			}
		}
	};
	let state = {
		privacyPolicy: true,
	}
	let jsx = testFunctions.renderPrivacyPolicy(data, state, proSettings);

	expect(jsx.props.children.type === 'div').toBeTruthy();
	state.privacyPolicy = false;
	jsx = testFunctions.renderPrivacyPolicy(data, state, proSettings);
	expect(jsx.props.children === '').toBeTruthy();

	proSettings.privacyPolicy.text = '';
	jsx = testFunctions.renderPrivacyPolicy(data, state, proSettings);
	expect(jsx.props.children === '').toBeTruthy();
} )


test( 'render content elements design preview', () => {

	let jsx = testFunctions.renderContentElementsDesignPreview(  '', {
		componentName: 'hello',
	}, {display: 'none'}, () => { return '' }, () => {}, {}, () => {}, 1 );

	expect(jsx).toBe('');

	jsx = testFunctions.renderContentElementsDesignPreview(  '', {
		componentName: 'ImageQuestion',
		answers: [
			{
				imageUrl: 'http://www.google.com/images/car.jpg'
			},
		]
	}, {display: 'none'}, () => { return '' }, () => {}, {}, () => {}, 1 );

	expect(jsx.props.className).toBe('surveyfunnel-lite-tab-ImageQuestion');

	jsx = testFunctions.renderContentElementsDesignPreview(  '', {
		componentName: 'TextElement',
	}, {display: 'none'}, () => { return '' }, () => {}, {}, () => {}, 1 );

	expect(jsx.props.className).toBe('surveyfunnel-lite-tab-TextElement');

} );

test('calling render content elements as per element type', () => {
	let data = testFunctions.callRenderContentElements('', () => { return 'HelloWorld' }, {
		componentName: 'TextElement',
	}, 10);

	expect(data).toBe('HelloWorld');
	data = testFunctions.callRenderContentElements('', () => { return 'HelloWorld' }, {
		componentName: 'ImageQuestion',
	}, 10);

	expect(data).toBe('HelloWorld');

	data = testFunctions.callRenderContentElements('', () => { return 'HelloWorld' }, {
		componentName: 'SomethingElse',
	}, 10);

	expect(data).toBe('');
})

test('returning tab number as per conditional logic', () => {
	const componentList = [
		{
			type: "START_ELEMENTS",
			privacyPolicy: false,
			id: "eakfgfetgukw0cnlvf",
			componentName: "CoverPage",
			type: "START_ELEMENTS",
			currentlySaved: true,
		}, 
		{
			title: "Single Choice",
			answers: [
				{
					checked: true,
					name: 'SC1'
				},
				{
					checked: false,
					name: 'SC2'
				}
			],
			value: "SC1",
			id: "hmakfih8n5ikw0cnz78",
			componentName: "SingleChoice",
			type: "CONTENT_ELEMENTS",
			conditions: [{
				jumpCondition: null,
				jumpQuestion: {
					id: "5zo3c692k2gkw0cp2ho",
					type: "CONTENT_ELEMENTS",
					title: "Short Answer",
					componentName: "ShortAnswer"
				},
				jumpOption: "SC1",
				conditionOn: true,
			}]
		},
		{
			title: 'Multi Choice',
			answers: [
				{
					checked: true,
					name: 'MC1',
				},
				{
					checked: true,
					name: 'MC2'
				}
			],
			id: "2jsi6lcis6kw0cofln",
			componentName: "MultiChoice",
			type: "CONTENT_ELEMENTS",
			conditions: [
				{		
				jumpCondition: "all",
				jumpQuestion: {
					id: "hmakfih8n5ikw0cnz78",
					type: "CONTENT_ELEMENTS",
					title: "Single Choice",
					componentName: "SingleChoice"
				},
				jumpOption: ["MC1", "MC2"],
				conditionOn: true,
				}
			]
		},
		{
			id: "5zo3c692k2gkw0cp2ho",
			type: "CONTENT_ELEMENTS",
			title: "Short Answer",
			componentName: "ShortAnswer",
			conditions: [
				{
					conditionOn: true,
					jumpCondition: "null",
					jumpOption: null,
					jumpQuestion: {
						id: "d5pbt9unandkw1rtfoq",
						type: "RESULT_ELEMENTS",
						title: "Conditional Results Page",
						componentName: "ResultScreen",
					}
				}
			]
		},
		{
			title: "Single Choice",
			answers: [
				{
					checked: true,
					name: 'SC1'
				},
				{
					checked: false,
					name: 'SC2'
				}
			],
			value: "SC1",
			id: "hmakfih8n5ikw0cnz78",
			componentName: "SingleChoice",
			type: "CONTENT_ELEMENTS",
		},
		{
			id: "d5pbt9unandkw1rtfoq",
			type: "RESULT_ELEMENTS",
			title: "Conditional Results Page",
			componentName: "ResultScreen",
		}
	];
	let data = testFunctions.changeCurrentTabAsPerConditionalLogic(1, componentList, 0);
	expect(data).toBe(0);

	data = testFunctions.changeCurrentTabAsPerConditionalLogic(1,  componentList, 1)
	expect(data).toBe(3);

	data = testFunctions.changeCurrentTabAsPerConditionalLogic(1, componentList, 3  );
	expect(data).toBe(5);
})