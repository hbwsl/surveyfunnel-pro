<?php
/**
 * Fired during plugin deactivation
 *
 * @link       https://club.wpeka.com
 * @since      1.0.0
 *
 * @package    Surveyfunnel_Pro
 * @subpackage Surveyfunnel_Pro/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Surveyfunnel_Pro
 * @subpackage Surveyfunnel_Pro/includes
 * @author     WPeka Club <support@wpeka.com>
 */
class Surveyfunnel_Pro_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
        update_option( 'surveyfunnel_pro_activated', false );
	}

}
